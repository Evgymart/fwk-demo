<?php

require_once "../vendor/autoload.php";

use Fwk\Core\Essential\Application;

$app = new Application(dirname(__FILE__), 'Fwk\Demo');

$app->registerRoutes('routes.php');
$app->run();
