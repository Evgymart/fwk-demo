<?php

namespace Fwk\Demo\Controllers;

use Fwk\Core\Essential\View;
use Fwk\Core\Base\Controller as BaseController;

class TestController extends BaseController
{
	public function index(array $params): string
	{
		$test = $params['param'] ?? 'test';
		return "Index: $test\n";
	}

	public function stuff(): View
	{
		$view = new View("View.php");
		return $view;
	}
}
