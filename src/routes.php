<?php

return [
	'GET' => [
		'/hello' => [
			'function' => function(array $args) { echo 'Hello, ' . ($args['blank'] ?? 'World'); }
		],
		'/test' => [
			'controller' => [
				'class' => 'TestController',
				'method' => 'index'
			],
		],
		'/test/stuff' => [
			'controller' => [
				'class' => 'TestController',
				'method' => 'stuff',
			]
		],
	],
];
